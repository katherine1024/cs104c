\documentclass[t, handout]{beamer}

\usepackage[utf8]{inputenc}
\usepackage{minted}
\usepackage{tikz}

\title{Segment Trees}
\author{Aditya Arjun, Steph Cheng}
\institute{CS 104C}
\date{Fall 2022}

\setbeamertemplate{footline}[frame number]{}

\setbeamertemplate{navigation symbols}{}

\begin{document}
 
\frame{\titlepage}
 
\begin{frame}
  \frametitle{Computing Sums}
  \begin{itemize}
  \item \textbf{Problem: } Given an array $a$ which has $n$ integers. 
    \pause
  \item Given $q$ queries of the form $[l, r]$ (1-indexed, both inclusive).
    \pause
  \item For each query, return $a[l] + a[l + 1] + \cdots + a[r]$.
    \pause
  \item What's the fast solution (and its runtime)?
    \pause
  \item Use prefix sums, which solves this problem in $O(n+q)$.
  \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Computing Sums, Part 2}
    \begin{itemize}
        \item \textbf{Problem: } Given an array $a$ which has $n$ integers. 
        \pause
        \item Given $q$ queries:
        \pause
        \begin{itemize}
          \item Type 1: Given $[l,r]$, find the sum of $a[l] + \cdots + a[r]$.
            \pause
          \item Type 2: Given $i$ and $x$, set $a[i] = x$.
            \pause
        \end{itemize}
        \item What happens if we try to do prefix sums here?
    \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Runtime Comparisons}
  \begin{itemize}
  \item Na\"ive solution is very good at updates, but bad at queries.
    \pause
  \item Prefix sums are very good at queries, but bad at updates.
    \pause
  \item Can we somehow combine the two and get something that is alright in both cases?
    \pause
  \end{itemize}
  \begin{center}
    \begin{tabular}{c|c|c} 
      & Query & Update \\
      \hline
      Na\"ive & $O(N)$ & $O(1)$ \\
      \hline
      Prefix Sums & $O(1)$ & $O(N)$ \\
      \hline
      ??? & $O(\sqrt{N})$ & $O(\sqrt{N})$ \\
    \end{tabular}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Bucketing}
  \begin{figure}
    \centering

    \begin{tikzpicture}
      \draw[step=2cm,gray,very thin] (0, 1) grid (8, 2);
      \draw[step=1cm,gray,very thin] (0, 0) grid (8, 1);
      
      \node at (0.5, 0.5) {$a[1]$};
      \node at (1.5, 0.5) {$a[2]$};
      \node at (2.5, 0.5) {$a[3]$};
      \node at (3.5, 0.5) {$a[4]$};
      \node at (4.5, 0.5) {$a[5]$};
      \node at (5.5, 0.5) {$a[6]$};
      \node at (6.5, 0.5) {$a[7]$};
      \node at (7.5, 0.5) {$a[8]$};
    \end{tikzpicture}
  \end{figure}
  \begin{itemize}
  \item Suppose we split the array into buckets of size $B$.
    \pause
  \item For each bucket, we store the sum of its elements.
    \pause
  \item What's the runtime for updating?
    \pause
  \item $O(B)$ to just recompute the sum of a single bucket.
    \pause
  \item How do we answer queries?
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Bucketing}
  \begin{figure}
    \centering

    \begin{tikzpicture}
      \draw[step=2cm,gray,very thin] (0, 1) grid (8, 2);
      \draw[step=1cm,gray,very thin] (0, 0) grid (8, 1);
      \draw[step=2cm,red,very thin] (2, 1) grid (6, 2);
      \draw[step=1cm,red,very thin] (1, 0) grid (2, 1);
      \draw[step=1cm,red,very thin] (6, 0) grid (7, 1);
      
      \node at (0.5, 0.5) {$a[1]$};
      \node at (1.5, 0.5) {$a[2]$};
      \node at (2.5, 0.5) {$a[3]$};
      \node at (3.5, 0.5) {$a[4]$};
      \node at (4.5, 0.5) {$a[5]$};
      \node at (5.5, 0.5) {$a[6]$};
      \node at (6.5, 0.5) {$a[7]$};
      \node at (7.5, 0.5) {$a[8]$};
    \end{tikzpicture}
  \end{figure}
  \begin{itemize}
    \item Each query covers many whole buckets, but will only use two partial
      buckets.
      \pause
    \item Runtime is $O(\frac{N}{B} + B)$.
      \pause
    \item What should we make $B$ to optimize our runtime?
      \pause
    \item With $B = \sqrt{N}$, updates and queries are both $O(\sqrt{N})$.
      \pause
    \item This technique is known as Square Root Decomposition.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Variations on Range Queries}
  \begin{itemize}
  \item Instead of computing the sum, consider these variations:
    \pause
    \begin{itemize}
    \item Find the product. 
      \pause
    \item Find $\min(a[l], \ldots, a[r])$.
      \pause
    \item Find $\text{MEX}(a[l], \ldots, a[r])$. MEX
      means the smallest positive integer that is \textbf{not} present in the
      set, e.g. $\text{MEX}(1,2,4,5) = 3$.
      \pause
    \end{itemize}
  \item \textbf{Recall:} Why can't we do these with prefix sums?
    \pause
  \item \textbf{Question:} Which of these can we do with Square Root Decomposition?
    \pause
  \item \textbf{Answer:} The first two work, the last one doesn't.
    \pause
  \item Square Root Decomposition does \textbf{not} need invertibility!
    \pause
  \item Operation still needs to be associative
    \pause
  \begin{itemize}
    \item $(a+b)+c = a+(b+c)$
    \item $\min(\min(a,b),c) = \min(a, \min(b,c))$
  \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Runtime Comparisons, Revisited}
  \begin{itemize}
  \item Square Root Decomposition provides a happy medium between the two.
    \pause
  \item In general, computing auxillary information helps us answer queries faster.
    \pause
  \item If we use more memory, could we do even better?
    \pause
  \end{itemize}
  \begin{center}
    \begin{tabular}{c|c|c|c} 
      & Query & Update & Memory \\
      \hline
      Na\"ive & $O(N)$ & $O(1)$ & $N$ \\
      \hline
      Prefix Sums & $O(1)$ & $O(N)$ & $2N$\\
      \hline
      Square Root Decomposition & $O(\sqrt{N})$ & $O(\sqrt{N})$ & $N+\sqrt{N}$\\
      \hline
      ??? & $O(\log{N})$ & $O(\log{N})$ & $N\log{N}$\\
    \end{tabular}
  \end{center}
\end{frame}

\begin{frame}
    \frametitle{Segment Trees}
    
    \begin{figure}
        \centering

        \begin{tikzpicture}
            \draw[step=8cm,gray,very thin] (0, 3) rectangle (8, 4);
            \draw[step=4cm,gray,very thin] (4, 2) rectangle (8, 3);
            \draw[step=4cm,gray,very thin] (0, 2) rectangle (4, 3);
            \draw[step=2cm,gray,very thin] (0, 1) grid (8, 2);
            \draw[step=1cm,gray,very thin] (0, 0) grid (8, 1);
                        
            \node at (0.5, 0.5) {$a[1]$};
            \node at (1.5, 0.5) {$a[2]$};
            \node at (2.5, 0.5) {$a[3]$};
            \node at (3.5, 0.5) {$a[4]$};
            \node at (4.5, 0.5) {$a[5]$};
            \node at (5.5, 0.5) {$a[6]$};
            \node at (6.5, 0.5) {$a[7]$};
            \node at (7.5, 0.5) {$a[8]$};
        \end{tikzpicture}
    \end{figure}
    
    \begin{itemize}
        \item Instead of having one set of blocks, we have a few ``layers''.
        \pause
        \item The first layer is one block.
        \pause
        \item Other layers split each block in the previous layer in half.
        \pause
        \item How many blocks are there in total? \pause $O(N)$
        \pause
        \item How many blocks is each array element in? \pause $O(\log N)$ 
        \pause
        \item What's the runtime to compute a sum? \pause $O(\log N)$
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Segment Trees}
    
    \begin{figure}
        \centering

        \begin{tikzpicture}
            \draw[step=8cm,gray,very thin] (0, 3) rectangle (8, 4);
            \draw[step=4cm,gray,very thin] (4, 2) rectangle (8, 3);
            \draw[step=4cm,gray,very thin] (0, 2) rectangle (4, 3);
            \draw[step=2cm,gray,very thin] (0, 1) grid (8, 2);
            \draw[step=1cm,gray,very thin] (0, 0) grid (8, 1);
                        
            \node at (0.5, 0.5) {$a[1]$};
            \node at (1.5, 0.5) {$a[2]$};
            \node at (2.5, 0.5) {$a[3]$};
            \node at (3.5, 0.5) {$a[4]$};
            \node at (4.5, 0.5) {$a[5]$};
            \node at (5.5, 0.5) {$a[6]$};
            \node at (6.5, 0.5) {$a[7]$};
            \node at (7.5, 0.5) {$a[8]$};
        \end{tikzpicture}
    \end{figure}
    
    \begin{itemize}
        \item The above structure is a binary tree called a \textbf{Segment Tree}.
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Implementation: Query}
\begin{itemize}
    \item Represent tree as an array; \texttt{tree[0]} is the root, and node $i$'s children are $2*i + 1$ and $2 * i + 2$.
    \item $i$ is the node in the tree
    \item $[l, r]$ is the interval that node $i$ contains the sum of.
    \item $[a, b]$ is the query interval.
    \item Returns the part of the interval sum contained in the subtree at node $i$
\end{itemize}
\begin{minted}{java}
    int[2 * N] tree;
    int query(int i, int l, int r, int a, int b) {
        if (r < a || l > b) return 0;
        if (a <= l && r <= b) return tree[i];
        return query(2 * i + 1, l, (l + r) / 2, a, b) +
            query(2 * i + 2, (l + r) / 2 + 1, r, a, b);
    }
\end{minted}
\pause
\begin{itemize}
    \item Why is the tree of size $2 * N$?
    \pause
    \item What if $N$ is not a power of 2?
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Implementation: Update}
\begin{itemize}
    \item $i$ is the node in the tree
    \item $[l, r]$ is the interval that node $i$ contains the sum of.
    \item $j$ is the index we want to change.
    \item $v$ is the value we want to change $A[j]$ to.
    \item Recursive function returns value of \texttt{tree[i]} after applying \texttt{A[j] = v}
\end{itemize}
\begin{minted}{java}
    int update(int i, int l, int r, int v, int j) {
        if (j < l || j > r) return tree[i];
        if (l == r) {
            tree[i] = v
        } else {
            tree[i] =
                update(2 * i + 1, l, (l + r) / 2, j, v) +
                update(2 * i + 2, (l + r) / 2 + 1, r, j, v);
        };
        return tree[i];
    }
\end{minted}
\end{frame}

\end{document}

%%% Local Variables:
%%% LaTeX-command: "latex -shell-escape"
%%% End:
