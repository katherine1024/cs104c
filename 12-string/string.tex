\documentclass[t,handout]{beamer}

\usepackage[utf8]{inputenc}
\usepackage{minted}
\usepackage{tikz}

\title{String Algorithms}
\author{Steph Cheng}
\institute{CS 104C}
\date{Fall 2022}

\setbeamertemplate{footline}[frame number]{}

\setbeamertemplate{navigation symbols}{}

\begin{document}
 
\frame{\titlepage}
 
\begin{frame}
  \frametitle{Finding Substrings}
  \begin{itemize}
    \item \textbf{Problem:} Given strings $S$ and $T$, determine if $T$ appears as
      a substring of $S$.
      \pause
    \item What's the na\"ive solution?
      \pause
    \item \textbf{Solution:} Check all substrings of length $|T|$, character by character.
      \pause
    \item What's the runtime?
      \pause
    \item Checking a substring takes $O(|T|)$ time, so $O(|S||T|)$ in total.
      \pause
    \item This works when $S$ and $T$ are small. How do we optimize this better?
      \pause
    \item \textbf{Goal:} Check substrings in faster than $O(|T|)$ time.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Hashing}
  \begin{itemize}
  \item We use hashing in sets and maps to compute ``equality'' in $O(1)$.
    \pause
  \item Let's try to use hashing to check substring equality in $O(1)$:
  \end{itemize}
\begin{minted}{java}
boolean is_substring(String s, String t) {
    HashSet<String> substrings = new HashSet<>();
    for(int i = 0; i < s.length()-t.length(); i++)
        substrings.add(s.substring(i, i+t.length()));
    return substrings.contains(t);
}
\end{minted}
  \begin{itemize}
      \pause
    \item What's the runtime?
      \pause
    \item $O(|S|)$? Why does this feel wrong?
      \pause
    \item This is actually still $O(|S||T|)$, as it takes $O(|T|)$ time to build
      and hash each substring of length $|T|$.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Sliding Window?}
  \begin{itemize}
  \item We can view our substring as sliding a window of size $|T|$ across
    $S$.
    \pause
  \item Consider the string 12345.
    \pause
  \item When going from 123 to 234, we can think of it in
    terms of operations:
    \pause
  \begin{itemize}
    \item Remove 1 from the front of the window.
      \pause
    \item Slide the window.
      \pause
    \item Add 4 to the back of the window.
      \pause
  \end{itemize}
  \item What is the goal here? Compute the hash of each substring quickly.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Visualizing the Window}
  \begin{figure}
    \centering

    \begin{tikzpicture}
      \draw[step=1cm,gray,very thin] (0, 0) grid (3, 1);
      \draw[step=1cm,gray,very thin] (0, 2) grid (3, 3);
      \draw[step=1cm,gray,very thin] (0, 4) grid (3, 5);
      \draw[step=1cm,gray,very thin] (0, 6) grid (3, 7);

      \node at (0.5, 6.5) {$1$}; \node at (1.5, 6.5) {$2$}; \node at (2.5, 6.5) {$3$};
      \node at (0.5, 4.5) {   }; \node at (1.5, 4.5) {$2$}; \node at (2.5, 4.5) {$3$};
      \node at (0.5, 2.5) {$2$}; \node at (1.5, 2.5) {$3$}; \node at (2.5, 2.5) {   };
      \node at (0.5, 0.5) {$2$}; \node at (1.5, 0.5) {$3$}; \node at (2.5, 0.5) {$4$};
    \end{tikzpicture}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Hashing  Numbers}
  \begin{itemize}
  \item For our numbers example, we can just use the number itself as the hash!
    \pause
  \item The positions in the window correspond to digits:
    \pause
  \begin{itemize}
    \item $\text{Hash}(123) = 1\cdot10^2 + 2 \cdot 10^1 + 3 \cdot 10^0$
      \pause
    \item $\text{Hash}(234) = 2\cdot10^2 + 3 \cdot 10^1 + 4 \cdot 10^0$
      \pause
  \end{itemize}
  \item The ``front'' of the window uses $10^{|W|-1}$, the ``back'' of the
    window uses $10^0$.
    \pause
  \item To go from $123$ from $234$:
    \pause
  \begin{itemize}
    \item Remove 1 from the front: $(1 \cdot 10^2 + 2 \cdot 10^1 + 3 \cdot 10^0)
      - 1 \cdot 10^2 = 2 \cdot 10^1 + 3 \cdot 10^0$
      \pause
    \item Slide the window: $(2 \cdot 10^1 + 3 \cdot 10^0) \cdot 10 = 2 \cdot
      10^2 + 3 \cdot 10^1$
      \pause
    \item Add 4 to the back: $2 \cdot 10^2 + 3 \cdot 10^1 + 4 \cdot 10^0$
  \end{itemize}
  \pause
  \item \textbf{Remark:} The window doesn't need to be fixed to perform these
    operations, as long as you keep track of the size of the window.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Hashing Strings}
  \begin{itemize}
  \item We can extend this to strings with letters, by just mapping
    letters to numbers!
    \pause
  \begin{itemize}
    \item a = 1, b = 2, \ldots, z = 26.
      \pause
    \end{itemize}
    \pause
  \item We can no longer use 10 as our base.
    \pause
  \begin{itemize}
    \item Hash(k) = $11 \cdot 10^0 = 11$
      \pause
    \item Hash(aa) = $1 \cdot 10^1 + 1 \cdot 10^0 = 11$
      \pause
    \item Use something bigger than all of our letters, such as $30$.
  \end{itemize}
  \item Hashes can overflow, so we need to store them mod $10^9+7$.
  \pause
  \item More generally, any big prime works, such as $998244353$.
  \pause
  \item For better accuracy, can store the hash as a pair, i.e. the value of
    the hash under \emph{two different} moduli.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Tries}
  \begin{itemize}
  \item Tries are a tree structure that stores many strings.
    \pause
  \item Each edge corresponds to a letter.
    \pause
  \item Let's add aaa, abc, and cat to a trie.
  \end{itemize}
  \begin{figure}
    \centering
    \begin{tikzpicture}[main/.style = {draw, circle}] 
      \pause
      \node[main,scale=1.5] (root) {};
      \pause
      \node[main,scale=1.5] (a) [above right of=root] {};
      \draw[->] (root) -- node[midway, above] {a} (a);
      \pause
      \node[main,scale=1.5] (aa) [right of=a] {};
      \draw[->] (a) -- node[midway, above] {a} (aa);
      \pause
      \node[main,scale=1.5] (aaa) [right of=aa] {};
      \draw[->] (aa) -- node[midway, above] {a} (aaa);
      \pause
      \node[main,scale=1.5] (ab) [below right of=a] {};
      \draw[->] (a) -- node[midway, above] {b} (ab);
      \pause
      \node[main,scale=1.5] (abc) [right of=ab] {};
      \draw[->] (ab) -- node[midway, above] {c} (abc);
      \pause
      \node[main,scale=1.5] (c) [below right of=root] {};
      \draw[->] (root) -- node[midway, above] {c} (c);
      \pause
      \node[main,scale=1.5] (ca) [right of=c] {};
      \draw[->] (c) -- node[midway, above] {a} (ca);
      \pause
      \node[main,scale=1.5] (cat) [right of=ca] {};
      \draw[->] (ca) -- node[midway, above] {t} (cat);
    \end{tikzpicture} 
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Using Tries}
  \begin{itemize}
  \item Tries can easily be used to reason about prefixes.
  \item For example, to test if $S$ is a prefix, just try traversing!
  \end{itemize}
  \begin{figure}
    \centering
    \begin{tikzpicture}[main/.style = {draw, circle}] 
      \node[main,scale=1.5] (root) {};
      \node[main,scale=1.5] (a) [above right of=root] {};
      \draw[->] (root) -- node[midway, above] {a} (a);
      \node[main,scale=1.5] (aa) [right of=a] {};
      \draw[->] (a) -- node[midway, above] {a} (aa);
      \node[main,scale=1.5] (aaa) [right of=aa] {};
      \draw[->] (aa) -- node[midway, above] {a} (aaa);
      \node[main,scale=1.5] (ab) [below right of=a] {};
      \draw[->] (a) -- node[midway, above] {b} (ab);
      \node[main,scale=1.5] (abc) [right of=ab] {};
      \draw[->] (ab) -- node[midway, above] {c} (abc);
      \node[main,scale=1.5] (c) [below right of=root] {};
      \draw[->] (root) -- node[midway, above] {c} (c);
      \node[main,scale=1.5] (ca) [right of=c] {};
      \draw[->] (c) -- node[midway, above] {a} (ca);
      \node[main,scale=1.5] (cat) [right of=ca] {};
      \draw[->] (ca) -- node[midway, above] {t} (cat);
    \end{tikzpicture} 
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Maximizing XOR}
  \begin{itemize}
  \item \textbf{Problem}: You have an array of $N$ integers, $a_1, \ldots,
    a_N$, and $Q$ queries $q_1, \ldots, q_Q$. For each query, output the maximum
    value of $q_i \oplus a_j$.
    \pause
  \item \textbf{Na\"ive Solution}: Try all pairs in $O(NQ)$ time.
    \pause
  \item \textbf{Observation}: If $q_i = 0$, the answer will just be the max $a_j$.
    \pause
  \item What if $q_i = 100\ldots00_2$? 
    \pause
  \item Anything starting with a 0 will have a higher XOR than anything starting
    with a 1.
    \pause
  \item What about $q_i = 110\ldots00_2$?
    \pause
  \item Best to worst prefixes: 00, 01, 10, 11.
    \pause
  \item \textbf{Solution}: Greedily ``build'' the answer based on highest order
    bits.
    \pause
  \item Use a trie for efficiency!
  \end{itemize}
\end{frame}
\begin{frame}
  \frametitle{Maximizing XOR}
  \begin{itemize}
  \item Array: $1100_2, 0001_2, 0110_2$, $q_i = 1010_2$.
  \item Current answer: $????_2$.
  \end{itemize}
  \begin{figure}
    \centering
    \begin{tikzpicture}[main/.style = {draw, circle}] 
      \node[main,scale=1.5,color=red] (root) {};
      \node[main,scale=1.5] (0) [above right of=root] {};
      \draw[->] (root) -- node[midway, above] {0} (0);
      \node[main,scale=1.5] (00) [right of=0] {};
      \draw[->] (0) -- node[midway, above] {0} (00);
      \node[main,scale=1.5] (000) [right of=00] {};
      \draw[->] (00) -- node[midway, above] {0} (000);
      \node[main,scale=1.5] (0001) [right of=000] {};
      \draw[->] (000) -- node[midway, above] {1} (0001);

      \node[main,scale=1.5] (01) [below right of=0] {};
      \draw[->] (0) -- node[midway, above] {1} (01);
      \node[main,scale=1.5] (011) [right of=01] {};
      \draw[->] (01) -- node[midway, above] {1} (011);
      \node[main,scale=1.5] (0110) [right of=011] {};
      \draw[->] (011) -- node[midway, above] {0} (0110);

      \node[main,scale=1.5] (1) [below right of=root] {};
      \draw[->] (root) -- node[midway, above] {1} (1);
      \node[main,scale=1.5] (11) [right of=1] {};
      \draw[->] (1) -- node[midway, above] {1} (11);
      \node[main,scale=1.5] (110) [right of=11] {};
      \draw[->] (11) -- node[midway, above] {0} (110);
      \node[main,scale=1.5] (1100) [right of=110] {};
      \draw[->] (110) -- node[midway, above] {0} (1100);
    \end{tikzpicture} 
  \end{figure}
\end{frame}
\begin{frame}
  \frametitle{Maximizing XOR}
  \begin{itemize}
  \item Array: $1100_2, 0001_2, 0110_2$, $q_i = 1010_2$.
  \item Current answer: $1???_2$.
  \end{itemize}
  \begin{figure}
    \centering
    \begin{tikzpicture}[main/.style = {draw, circle}] 
      \node[main,scale=1.5,color=red] (root) {};
      \node[main,scale=1.5,color=red] (0) [above right of=root] {};
      \draw[->,color=red] (root) -- node[midway, above] {0} (0);
      \node[main,scale=1.5] (00) [right of=0] {};
      \draw[->] (0) -- node[midway, above] {0} (00);
      \node[main,scale=1.5] (000) [right of=00] {};
      \draw[->] (00) -- node[midway, above] {0} (000);
      \node[main,scale=1.5] (0001) [right of=000] {};
      \draw[->] (000) -- node[midway, above] {1} (0001);

      \node[main,scale=1.5] (01) [below right of=0] {};
      \draw[->] (0) -- node[midway, above] {1} (01);
      \node[main,scale=1.5] (011) [right of=01] {};
      \draw[->] (01) -- node[midway, above] {1} (011);
      \node[main,scale=1.5] (0110) [right of=011] {};
      \draw[->] (011) -- node[midway, above] {0} (0110);

      \node[main,scale=1.5] (1) [below right of=root] {};
      \draw[->] (root) -- node[midway, above] {1} (1);
      \node[main,scale=1.5] (11) [right of=1] {};
      \draw[->] (1) -- node[midway, above] {1} (11);
      \node[main,scale=1.5] (110) [right of=11] {};
      \draw[->] (11) -- node[midway, above] {0} (110);
      \node[main,scale=1.5] (1100) [right of=110] {};
      \draw[->] (110) -- node[midway, above] {0} (1100);
    \end{tikzpicture} 
  \end{figure}
\end{frame}
\begin{frame}
  \frametitle{Maximizing XOR}
  \begin{itemize}
  \item Array: $1100_2, 0001_2, 0110_2$, $q_i = 1010_2$.
  \item Current answer: $11??_2$.
  \end{itemize}
  \begin{figure}
    \centering
    \begin{tikzpicture}[main/.style = {draw, circle}] 
      \node[main,scale=1.5,color=red] (root) {};
      \node[main,scale=1.5,color=red] (0) [above right of=root] {};
      \draw[->,color=red] (root) -- node[midway, above] {0} (0);
      \node[main,scale=1.5] (00) [right of=0] {};
      \draw[->] (0) -- node[midway, above] {0} (00);
      \node[main,scale=1.5] (000) [right of=00] {};
      \draw[->] (00) -- node[midway, above] {0} (000);
      \node[main,scale=1.5] (0001) [right of=000] {};
      \draw[->] (000) -- node[midway, above] {1} (0001);

      \node[main,scale=1.5,color=red] (01) [below right of=0] {};
      \draw[->,color=red] (0) -- node[midway, above] {1} (01);
      \node[main,scale=1.5] (011) [right of=01] {};
      \draw[->] (01) -- node[midway, above] {1} (011);
      \node[main,scale=1.5] (0110) [right of=011] {};
      \draw[->] (011) -- node[midway, above] {0} (0110);

      \node[main,scale=1.5] (1) [below right of=root] {};
      \draw[->] (root) -- node[midway, above] {1} (1);
      \node[main,scale=1.5] (11) [right of=1] {};
      \draw[->] (1) -- node[midway, above] {1} (11);
      \node[main,scale=1.5] (110) [right of=11] {};
      \draw[->] (11) -- node[midway, above] {0} (110);
      \node[main,scale=1.5] (1100) [right of=110] {};
      \draw[->] (110) -- node[midway, above] {0} (1100);
    \end{tikzpicture} 
  \end{figure}
\end{frame}
\begin{frame}
  \frametitle{Maximizing XOR}
  \begin{itemize}
  \item Array: $1100_2, 0001_2, 0110_2$, $q_i = 1010_2$.
  \item Current answer: $110?_2$.
  \end{itemize}
  \begin{figure}
    \centering
    \begin{tikzpicture}[main/.style = {draw, circle}] 
      \node[main,scale=1.5,color=red] (root) {};
      \node[main,scale=1.5,color=red] (0) [above right of=root] {};
      \draw[->,color=red] (root) -- node[midway, above] {0} (0);
      \node[main,scale=1.5] (00) [right of=0] {};
      \draw[->] (0) -- node[midway, above] {0} (00);
      \node[main,scale=1.5] (000) [right of=00] {};
      \draw[->] (00) -- node[midway, above] {0} (000);
      \node[main,scale=1.5] (0001) [right of=000] {};
      \draw[->] (000) -- node[midway, above] {1} (0001);

      \node[main,scale=1.5,color=red] (01) [below right of=0] {};
      \draw[->,color=red] (0) -- node[midway, above] {1} (01);
      \node[main,scale=1.5,color=red] (011) [right of=01] {};
      \draw[->,color=red] (01) -- node[midway, above] {1} (011);
      \node[main,scale=1.5] (0110) [right of=011] {};
      \draw[->] (011) -- node[midway, above] {0} (0110);

      \node[main,scale=1.5] (1) [below right of=root] {};
      \draw[->] (root) -- node[midway, above] {1} (1);
      \node[main,scale=1.5] (11) [right of=1] {};
      \draw[->] (1) -- node[midway, above] {1} (11);
      \node[main,scale=1.5] (110) [right of=11] {};
      \draw[->] (11) -- node[midway, above] {0} (110);
      \node[main,scale=1.5] (1100) [right of=110] {};
      \draw[->] (110) -- node[midway, above] {0} (1100);
    \end{tikzpicture} 
  \end{figure}
\end{frame}
\begin{frame}
  \frametitle{Maximizing XOR}
  \begin{itemize}
  \item Array: $1100_2, 0001_2, 0110_2$, $q_i = 1010_2$.
  \item Current answer: $1100_2$.
  \end{itemize}
  \begin{figure}
    \centering
    \begin{tikzpicture}[main/.style = {draw, circle}] 
      \node[main,scale=1.5,color=red] (root) {};
      \node[main,scale=1.5,color=red] (0) [above right of=root] {};
      \draw[->,color=red] (root) -- node[midway, above] {0} (0);
      \node[main,scale=1.5] (00) [right of=0] {};
      \draw[->] (0) -- node[midway, above] {0} (00);
      \node[main,scale=1.5] (000) [right of=00] {};
      \draw[->] (00) -- node[midway, above] {0} (000);
      \node[main,scale=1.5] (0001) [right of=000] {};
      \draw[->] (000) -- node[midway, above] {1} (0001);

      \node[main,scale=1.5,color=red] (01) [below right of=0] {};
      \draw[->,color=red] (0) -- node[midway, above] {1} (01);
      \node[main,scale=1.5,color=red] (011) [right of=01] {};
      \draw[->,color=red] (01) -- node[midway, above] {1} (011);
      \node[main,scale=1.5,color=red] (0110) [right of=011] {};
      \draw[->,color=red] (011) -- node[midway, above] {0} (0110);

      \node[main,scale=1.5] (1) [below right of=root] {};
      \draw[->] (root) -- node[midway, above] {1} (1);
      \node[main,scale=1.5] (11) [right of=1] {};
      \draw[->] (1) -- node[midway, above] {1} (11);
      \node[main,scale=1.5] (110) [right of=11] {};
      \draw[->] (11) -- node[midway, above] {0} (110);
      \node[main,scale=1.5] (1100) [right of=110] {};
      \draw[->] (110) -- node[midway, above] {0} (1100);
    \end{tikzpicture} 
  \end{figure}
\end{frame}
\end{document}

%%% Local Variables:
%%% LaTeX-command: "latex -shell-escape"
%%% End: