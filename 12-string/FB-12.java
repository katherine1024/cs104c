import java.util.*;
import java.io.*;

// https://www.spoj.com/problems/ADACLEAN/
// I'm passing the sample and a bunch of small cases I've written by hand... Can you help me?

public class FindTheBug12 {
    public static void main(String[] args) {
        FastScanner in = new FastScanner();
        PrintWriter out = new PrintWriter(System.out);
        int numCases = in.nextInt();
        for (int t=0; t<numCases; t++) {
            int strLen = in.nextInt();
            int substrLen = in.nextInt();
            String s = in.next();

            // Store all the hashes of each substring of length K
            HashSet<Long> substrHashes = new HashSet<>();

            // compute the hash of the first K characters
            // also compute 27^(K-1) along the way
            long hash = 0;
            long biggest_power = 1;
            for(int i = 0; i < substrLen; i++) {
                // convert 'a'...'z' -> 1...26
                int letter = s.charAt(i) - 96;
                hash = 27*hash + letter;
                biggest_power *= 27;
            }
            // we just computed 27^K above
            biggest_power /= 27;

            substrHashes.add(hash);

            // use sliding window to compute the other hashes
            for(int i = substrLen; i < strLen; i++) {
                // remove the leftmost letter
                int left_letter = s.charAt(i-substrLen) - 96;
                hash -= left_letter * biggest_power;

                // slide the window and add the new letter
                int right_letter = s.charAt(i) - 96;
                hash = 27*hash + right_letter;

                // add hash to the set
                substrHashes.add(hash);
            }
            out.println(substrHashes.size());
        }
        out.flush();
    }

    static class FastScanner {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
        public FastScanner() {
            reader = new BufferedReader(new InputStreamReader(System.in), 32768);
            tokenizer = null;
        }
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
        public int nextInt() {
            return Integer.parseInt(next());
        }
        public long nextLong() {
            return Long.parseLong(next());
        }
        public double nextDouble() {
            return Double.parseDouble(next());
        }
        public String nextLine() {
            try {
                return reader.readLine();
            } catch(IOException e) {
                throw new RuntimeException(e);
            }
        }
        public String restLine() {
            String s = "";
            while(tokenizer.hasMoreTokens()) s += tokenizer.nextToken() + " ";
            return s.trim();
        }
    }
}
