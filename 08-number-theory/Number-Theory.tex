\documentclass[t]{beamer}

\usepackage[utf8]{inputenc}
\usepackage{minted}
\usepackage{graphicx}

\title{Number Theory: Mods and Primes}
\author{Aditya Arjun, Steph Cheng}
\institute{CS 104C}
\date{Fall 2022}

\setbeamertemplate{footline}[frame number]{}

\setbeamertemplate{navigation symbols}{}

\begin{document}
 
\frame{\titlepage}

\begin{frame}
  \frametitle{Division Algorithm}
  \begin{itemize}
  \item For every pair of integers $a, b$, there exist unique integers $q, r$ such that $a = q b + r$ and $0 \leq r < b$.
    \pause
    As an example, if $a = 11, b = 5$, $a = 2 b + 1$. The quotient is $q = 2$, and the remainder is $r = 1$.
    \pause 
  \item In most languages, $a / b = q$, $a \% b = r$.
    \pause
  \item Be careful with negative values!
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Definitions}
    
    \begin{itemize}
        \item $d$ is a \textbf{divisor} of $n$ if $d$ divides $n$ evenly.
        \pause
        \item Equivalent: $n \% d = 0$

        \item A number is prime if it has exactly two positive divisors (1 and itself).
    \end{itemize}
    
    \begin{minted}{java}
    boolean isPrime(int n) {
        for (int d = 2; d < n; ++d)
            if (n % d == 0)
                return false;
        return true;
    }
    \end{minted}
    
    \pause
    \begin{itemize}
        \item Time complexity? \pause $O(N)$
        \pause
        \item Can we do better?
    \end{itemize}
    
\end{frame}

\begin{frame}[fragile]

    \frametitle{Primes}
    
    \begin{itemize}
        \item \textbf{Problem:} Given a number $N$, determine whether it is prime
        \item \textbf{Solution:} Check all possible divisors \textbf{up to $\sqrt{N}$}
    \end{itemize}
    
    \begin{minted}{java}
    boolean isPrime(int n) {
        for (int d = 2; d * d <= n; ++d)
            if (n % d == 0)
                return false;
        return true;
    }
    \end{minted}
    
    \pause
    \begin{itemize}
        \item Time complexity? \pause $O(\sqrt{N})$
    \end{itemize}
    
\end{frame}

\begin{frame}[fragile]

    \frametitle{Primes}
    
    \begin{itemize}
        \item \textbf{Problem:} Given a number $N$, get all primes up to $N$
        \pause
        \item \textbf{Solution:} Assume every number is prime. Eliminate all the multiples of $2$, then all the multiples of $3$, \dots
    \end{itemize}
    
    \begin{minted}{java}
    List<Integer> getPrimes(int n) {
        boolean composite = new boolean[n + 1];
        List<Integer> primes = new ArrayList<Integer>();
        for (int i = 2; i <= n; ++i) {
            if (composite[i]) continue;
            primes.add(i);
            for (int j = 2 * i; j <= n; j += i)
                composite[j] = true;
        }
        return primes;
    }
    \end{minted}
    
    \pause
    \begin{itemize}
        \item Time complexity? \pause $O(N \log \log N)$
    \end{itemize}
    
\end{frame}


\begin{frame}[fragile]

    \frametitle{GCDs}
    
    \begin{itemize}
        \item The greatest common divisor of two numbers $a$ and $b$ is the largest integer $g$ such that both $a$ and $b$ are multiples of $g$.
        
        \pause

        \item Complex Approach: factor both numbers
        
        \pause
        
        \item Euclidean algorithm: Let $a = q b + r$. Note that if some value $g$ divides both $a$ and $b$, then $g$ divides $r$.
        
        \pause

        Proof: $a = q b + r \implies r = a - qb$. Since $g$ divides both $a$ and $b$, $g$ divides $r$.
    \end{itemize}
    
    \pause
    \begin{minted}{java}
    int gcd(int a, int b) {
        if (b == 0) {
            return a;
        }
        return gcd(b, a % b);
    }
    \end{minted}
    
    \pause
    \begin{itemize}
        \item Time complexity? \pause $O(\log N)$
    \end{itemize}
\end{frame}
 
\begin{frame}
    \frametitle{Problems with Precision}
    \begin{itemize}
      \item Problem 1: Given an integer $N$ $(1 \leq N \leq 10)$, output $N!$
      \pause
      \item Problem 2: Given an integer $N$ $(1 \leq N \leq 10)$,
        output $\frac{1}{1} + \frac{1}{2^2} + \ldots + \frac{1}{N^N}$.
      \pause
      \item What goes wrong if we try to increase $N$ for these problems?
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Modular Arithmetic}
    \begin{itemize}
        \item Many problems will ask you to output an answer ``modulo $10^9 +
          7$''
        \pause
        \item We use this number because
        \begin{itemize}
            \item It is prime
            \pause
            \item It fits into an integer (INT\_MAX is around $2 \cdot 10^9$)
        \end{itemize}
        \pause
        \item How do we work with ``modulo''?
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Modular Arithmetic}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{center}
             \includegraphics[width=\textwidth]{clock.png}
             \end{center}
        \end{column}
        
        \begin{column}{0.5\textwidth}
        \begin{itemize}
            \item Clocks work ``modulo 12''.
            \pause
            \item What is 3 hours after 11? \pause 2.
            \pause
            \item Then we say $11 + 3 \equiv 2 \pmod{12}$.
            \pause
            \item What about 3 hours before 2? \pause 11.
            \pause
            \item Then we say $2-3 \equiv 11 \pmod{12}$.

        \end{itemize}

        \end{column}
    \end{columns}
  \end{frame}

\begin{frame}
    \frametitle{Modular Rules}
    Which of the following are correct?
    \begin{itemize}
        \item $(a + b) \% m = ((a \% m) + (b \% m)) \% m$ ?
        \pause
        \item $(a - b) \% m = ((a \%{m}) - (b \%{m})) \% m$ ?
        \pause
        \item $(a \cdot b) \%{m} = ((a \%{m}) \cdot (b \%{m})) \% m$ ?
        \pause
        \item $(a / b) \%{m} = ((a \%{m}) / (b \%{m})) \% m$ ?
        \pause
        \item $a^b \%{m} = (a \%{m})^b = (a \%{m})^{(b \%{m})} \% m$ ?
    \end{itemize}
    
    \pause
    Answers: First 3 are true, fourth makes little sense, first part of the last one is true.
    
    \pause
    Again, be careful with negatives and mod. We usually implement subtraction as (a \% m - b \% m + m) \% m. 
\end{frame}

\begin{frame}
  \frametitle{Factorial, Revisited}
  \begin{itemize}
  \item Problem: Given an integer $N$ $(1 \leq N \leq 10^6)$, output $N!$ modulo $10^9+7$.
  \pause
  \item Solution: Just multiply each number one by one, but take the result
    modulo $10^9+7$ after each multiplication.
  \pause
  \item This also computes $1!, \ldots, (N-1)!$ modulo $10^9+7$ along the way!
  \end{itemize}
  
\end{frame}

\begin{frame}[fragile]
    
    \frametitle{Modular rules, again}
    
    \begin{itemize}
        \item \textbf{Problem:} Given $N$ people, calculate how many ways you can make a committee of $M$ people.
        \pause
        \item \textbf{Output your answer modulo} $\mathbf{10^9 + 7}$.
        \pause
        \item How do we compute the answer to this?
    \end{itemize}

\end{frame}

\begin{frame}[fragile]
  
  \frametitle{Multiplicative Inverse}
  
  \begin{itemize}
  \item Order doesn't matter here! We can use binomial coefficients:
    \[\binom{N}{M} = \frac{N!}{M!(N-M)!}\]
  \pause
  \item \textbf{Problem:} Can't divide numbers under a modulus
  \pause
  \item We can think about division as multiplication by an inverse: $a/b = a
    \cdot b^{-1}$
  \pause
  \item Formally, a ``multiplicative inverse'' should satisfy $b \cdot b^{-1} =
    1$.
  \pause
  \item This idea extends to modulus: $3 \cdot 4 \equiv 1 \pmod{11}$, so $3^{-1} \equiv 4 \pmod{11}$.
  \pause
  \item Generalizing further, $2/3 = 2 \cdot 3^{-1}$, so $2/3 \equiv 8
    \pmod{11}$.
  \pause
  \item This satisfies the properties we want: $(2/3) \cdot 3 = 2$, $8 \cdot 3
    \equiv 2 \pmod{11}$.
  \end{itemize}

\end{frame}

\begin{frame}[fragile]
    
    \frametitle{Multiplicative Inverse}
    
    \begin{itemize}
        \item How do we compute multiplicative inverse?
        \pause
        \item We can use Fermat's little theorem: $b^{p-1} \equiv 1 \pmod{p}$
        \pause
        \item Then $b^{-1} = b^{p-2}$
        \pause
        \item \textbf{Problem:} $p$ is usually big, so just multiplying it is too slow. 
        \pause
        \item \textbf{Idea:} Exponentiation by squaring
        \pause
        \item Even case: $x^{2k} = (x^2)^k$
        \pause
        \item Odd case: $x^{2k+1} = x \cdot x^{2k} = x \cdot (x^2)^k$
        \pause
        \item Exponent halves every time, so $O(\log N)$.
    \end{itemize}

\end{frame}


\end{document}

%%% Local Variables:
%%% LaTeX-command: "latex -shell-escape"
%%% End: